namespace OglasnikApp {
  public partial class FormMain : Form {
    public FormMain() {
      InitializeComponent();
      Ads = new List<string>();
    }

    private void btnAdd_Click(object sender, EventArgs e) {
      if (!cbPicture.Checked || !cbVideo.Checked) {
        Ads.Add(string.Format("{0} {1}", tbTitle.Text, tbContent.Text));
        tbTitle.Text = "";
        tbContent.Text = "";
        RefreshAdsList();
      } else {
        MessageBox.Show("Morate odabrati ili slikovni ili video oglas!", "Gre�ka", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void RefreshAdsList() {
      lbAds.Items.Clear();
      foreach (var ad in Ads) {
        lbAds.Items.Add(ad);
      }
    }

    private List<string> Ads { get; set; }
  }
}